import mongoose from "mongoose";
const { Schema, model } = mongoose;

const UserSchema = new Schema(
    {
        first_name: { type: String, required: true },
        last_name: { type: String, required: true },
        email: {
            type: String,
            required: [true, "Email is required"],
            unique: true,
        },
        // phone: {
        //     type: String,
        //     validate: {
        //         validator: function(v: string) {
        //             return /^[0-9]{10}$/.test(v);
        //         },
        //         message: (value: string) => `${value} is not a valid phone number`
        //     },
        //     required: true
        // },
        phone: { type: String, required: true },
    },
    { timestamps: true }
);

export default model("user", UserSchema);
