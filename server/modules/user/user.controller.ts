import { Request, Response } from "express";
import { addUserValidation, updateUserValidation } from "./user.validation.js";
import UserService from "./user.service.js";

export const createUser = async (req: Request, res: Response) => {
    await addUserValidation(req.body);
    const user = await UserService.create(req.body);
    res.status(200).json(user);
};

export const getAllUsers = async (req: Request, res: Response) => {
    const users = await UserService.getAll();
    res.status(200).json(users);
};

export const getUser = async (req: Request, res: Response) => {
    const user = await UserService.getUser(req.params.id, req.path);
    res.status(200).json(user);
};

export const updateUser = async (req: Request, res: Response) => {
    await updateUserValidation(req.body);
    const user = await UserService.updateUser(
        req.params.id,
        req.path,
        req.body
    );
    res.status(200).json(user);
};

export const deleteUser = async (req: Request, res: Response) => {
    const user = await UserService.deleteUser(req.params.id, req.path);
    res.status(200).json(user);
};

export const paginate = async (req: Request, res: Response) => {
    const query = await UserService.paginate(req.params.page, req.params.num);
    res.status(200).json(query);
};
