/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
import raw from "../../middleware/route.async.wrapper.js";
import express from "express";
import {
    createUser,
    deleteUser,
    getAllUsers,
    getUser,
    paginate,
    updateUser,
} from "./user.controller.js";

const router = express.Router();

// parse json req.body on post routes
router.use(express.json());

// CREATES A NEW USER
router.post("/", raw(createUser));

// GET ALL USERS
router.get("/", raw(getAllUsers));

// GETS A SINGLE USER
router.get("/:id", raw(getUser));

// UPDATES A SINGLE USER
router.put("/:id", raw(updateUser));

// DELETES A USER
router.delete("/:id", raw(deleteUser));

// paginate
router.get("/paginate/:page/:num", raw(paginate));

export default router;
