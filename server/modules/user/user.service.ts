import HttpException from "../../exceptions/http.exception.js";
import UrlNotFoundException from "../../exceptions/urlNotFound.exception.js";
import { Iobj } from "../../typing.js";
import user_model from "./user.model.js";

class UserService {
    user_model = user_model;

    async create(obj: Iobj) {
        const exists = await user_model.findOne({ email: obj.email });
        if (exists) throw new HttpException(409, "user is already exists");
        const user = await user_model.create(obj);
        return user;
    }

    async getAll() {
        const users = await // .select(`-__v`);
        user_model.find().select(`-_id 
                                          first_name 
                                          last_name 
                                          email 
                                          phone`);
        return users;
    }

    async getUser(id: string, path: string) {
        const user = await user_model.findById(id).select(`-_id
            first_name
            last_name
            email
            phone`);
        if (!user) throw new UrlNotFoundException(`/api/users${path}`);
        return user;
    }

    async updateUser(id: string, path: string, obj: Iobj) {
        const user = await user_model.findByIdAndUpdate(id, obj, {
            new: true,
            upsert: false,
        });
        if (!user) throw new UrlNotFoundException(`/api/users${path}`);
        return user;
    }

    async deleteUser(id: string, path: string) {
        const user = await user_model.findByIdAndRemove(id);
        if (!user) throw new UrlNotFoundException(`/api/users${path}`);
        return user;
    }

    async paginate(page_str: string, num_str: string) {
        const num = Number(num_str);
        const docsToSkip = Number(page_str) * num;
        const query = await user_model.find().skip(docsToSkip).limit(num)
            .select(`-_id 
            first_name 
            last_name 
            email 
            phone`);
        return query;
    }
}

const instance = new UserService();
export default instance;
