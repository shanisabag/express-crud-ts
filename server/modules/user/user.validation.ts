import { addUserScheme, updateUserScheme } from "../../schemes/schemes.js";
import { Iobj } from "../../typing";

export async function validateUser(
    obj: Iobj,
    type: "ADD_USER" | "UPDATE_USER"
) {
    if (type === "ADD_USER") {
        await addUserScheme.validateAsync(obj);
    } else if (type === "UPDATE_USER") {
        await updateUserScheme.validateAsync(obj);
    }
}

export async function addUserValidation(obj: Iobj) {
    await validateUser(obj, "ADD_USER");
}

export async function updateUserValidation(obj: Iobj) {
    await validateUser(obj, "UPDATE_USER");
}
