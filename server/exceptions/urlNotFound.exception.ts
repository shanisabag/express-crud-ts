import HttpException from "./http.exception.js";

class UrlNotFoundException extends HttpException {
    constructor(path: string) {
        super(404, `the url ${path} was not found`);
    }
}

export default UrlNotFoundException;
