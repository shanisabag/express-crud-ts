// require('dotenv').config();
import express from "express";
import morgan from "morgan";
import log from "@ajar/marker";
import cors from "cors";

import { connect_db } from "./db/mongoose.connection.js";
import user_router from "./modules/user/user.router.js";

import {
    // error_handler,
    // error_handler2,
    // not_found,
    responseWithError,
    urlNotFound,
} from "./middleware/errors.handler.js";

// const {
//     PORT = 8080,
//     HOST = "localhost",
//     DB_URI = "mongodb://localhost:27017/crud-demo",
// } = process.env;

// const app = express();

// // middleware
// app.use(cors());
// app.use(morgan("dev"));

// // routing
// // app.use('/api/stories', story_router);
// app.use("/api/users", user_router);

// // // central error handling
// // app.use(error_handler);
// // app.use(error_handler2);

// // //when no routes were matched...
// // app.use("*", not_found);

// app.use("*", urlNotFound);
// app.use(responseWithError);

// //start the express api server
// (async () => {
//     //connect to mongo db
//     await connect_db(DB_URI as string);
//     app.listen(Number(PORT), HOST);
//     log.magenta("api is live on", ` ✨ ⚡  http://${HOST}:${PORT} ✨ ⚡`);
// })().catch(console.log);

// converting app to class
class startApp {
    private _PORT: number;
    private _HOST: string;
    private _DB_URI: string;
    private _app;

    constructor() {
        this._PORT = Number(process.env.PORT) || 8080;
        this._HOST = process.env.HOST || "localhost";
        this._DB_URI =
            process.env.DB_URI || "mongodb://localhost:27017/crud-demo";
        this._app = express();
    }

    init() {
        // middleware
        this._app.use(cors());
        this._app.use(morgan("dev"));

        this.routing();

        // error handling
        this.errorsHandler();

        this.connectToDB().catch(console.log);
    }

    private routing() {
        this._app.use("/api/users", user_router);
    }

    private errorsHandler() {
        this._app.use("*", urlNotFound);
        this._app.use(responseWithError);
    }

    private async connectToDB() {
        //connect to mongo db
        await connect_db(this._DB_URI);
        this._app.listen(this._PORT, this._HOST);
        log.magenta(
            "api is live on",
            ` ✨ ⚡  http://${this._HOST}:${this._PORT} ✨ ⚡`
        );
    }
}

const app = new startApp();
app.init();
