// import log from "@ajar/marker";
import { NextFunction, Request, Response } from "express";
import HttpException from "../exceptions/http.exception.js";
import UrlNotFoundException from "../exceptions/urlNotFound.exception.js";
// const { White, Reset, Red } = log.constants;
// const { NODE_ENV } = process.env;

// export const error_handler = (
//     err: Error,
//     req: Request,
//     res: Response,
//     next: NextFunction
// ) => {
//     log.green("error_handler");
//     log.error(err);
//     next(err);
// };

// export const error_handler2 = (
//     err: Error,
//     req: Request,
//     res: Response,
//     next: NextFunction
// ) => {
//     if (NODE_ENV !== "production")
//         res.status(500).json({ status: err.message, stack: err.stack });
//     else res.status(500).json({ status: "internal server error..." });
// };

// export const not_found = (req: Request, res: Response) => {
//     log.info(`url: ${White}${req.url}${Reset}${Red} not found...`);
//     res.status(404).json({ status: `url: ${req.url} not found...` });
// };

// url not found
export const urlNotFound = (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    next(new UrlNotFoundException(req.path));
};

// response with error
export const responseWithError = (
    err: HttpException | UrlNotFoundException,
    req: Request,
    res: Response,
    next: NextFunction
) => {
    const response = {
        status: err.status || 500,
        message: err.message || "something went wrong",
        stack: err.stack,
    };
    res.status(response.status).send({ response });
};
